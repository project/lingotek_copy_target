<?php

namespace Drupal\lingotek_copy_target\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigTargetsDeleteForm extends ConfirmFormBase {

  use ConfigFormBaseTrait;

  /**
   * The key to be deleted
   * @var int
   */
  protected $mapKey;

  /**
   * The mapping item to be deleted
   *
   * @var array
   */
  protected $mapping;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The configurable language manager.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigurableLanguageManagerInterface $language_manager) {
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lingotek_copy_target.mappings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $originalLangcode = $this->mapping['original_language'];
    $copyLangcode = $this->mapping['copy_language'];
    $languages = $this->languageManager->getLanguages();
    return $this->t('Are you sure you want to delete the mapping %original_language &rarr; %copy_language?', [
      '%original_language' => "{$languages[$originalLangcode]->getName()} ($originalLangcode)",
      '%copy_language' => "{$languages[$copyLangcode]->getName()} ($copyLangcode)",
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('lingotek_copy_target.config');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lingotek_copy_target_config_targets_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $map_key = NULL) {
    $this->mapKey = $map_key;
    $this->mapping = $this->config('lingotek_copy_target.mappings')->get("map.{$this->mapKey}");
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('lingotek_copy_target.mappings')->clear("map.{$this->mapKey}")->save();
    $args = [
      '%original' => $this->mapping['original_language'],
      '%target' => $this->mapping['copy_language'],
    ];
    $this->logger('lingotek_copy_target')->notice('The translation mapping for %original &rarr; %target has been removed', $args);
    $this->messenger()->addStatus($this->t('The translation mapping for %original &rarr; %target has been removed', $args));
    $form_state->setRedirect('lingotek_copy_target.config');
  }

}
