<?php

namespace Drupal\lingotek_copy_target\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Configuration for targets used in module
 */
class ConfigTargets extends ConfigFormBase {

  /**
   * The configurable language manager.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigurableLanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lingotek_copy_target_config_target';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['lingotek_copy_target.mappings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $languages = $this->languageManager->getLanguages();
    $existing_languages = [];
    foreach ($languages as $langcode => $language) {
      $existing_languages[$langcode] = "{$language->getName()} ({$langcode})";
    }

    $form['mappings'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Original language'),
        $this->t('Copy language'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No mappings available'),
    ];

    $mappings = $this->config('lingotek_copy_target.mappings')->get('map') ?? [];
    foreach ($mappings as $key => $languages) {
      $form['mappings'][$key] = [
        'original_language' => [
          '#title' => $this->t('Original language'),
          '#title_display' => 'invisible',
          '#type' => 'select',
          '#default_value' => $languages['original_language'],
          '#options' => [''] + $existing_languages,
          '#required' => TRUE,
        ],
        'copy_language' => [
          '#title' => $this->t('Copy language'),
          '#title_display' => 'invisible',
          '#type' => 'select',
          '#default_value' => $languages['copy_language'],
          '#options' => [''] + $existing_languages,
          '#required' => TRUE,
        ],
      ];

      $form['mappings'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => [],
      ];
      $form['mappings'][$key]['operations']['#links']['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('lingotek_copy_target.config_targets_delete', ['map_key' => $key]),
      ];
    }

    // Insert empty string to use as default value for new mappings
    $form['new_mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Add a new mapping'),
      '#tree' => TRUE,
    ];

    $form['new_mapping']['original_language'] = [
      '#id' => 'new_original_language',
      '#type' => 'select',
      '#title' => $this->t('Original language'),
      '#options' => [''] + $existing_languages,
      '#default_value' => '',
    ];
    $form['new_mapping']['copy_language'] = [
      '#id' => 'new_copy_language',
      '#type' => 'select',
      '#title' => $this->t('Copy language'),
      '#options' => [''] + $existing_languages,
      '#default_value' => '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $unique_values = [];
    if ($form_state->hasValue('mappings')) {
      $mappings = $form_state->getValue('mappings');
      \Drupal::logger('lingotek_copy_target')->info('mappings %m', ['%m' => print_r($mappings, TRUE)]);
      if (!empty($mappings)) {
        foreach ($mappings as $key => $data) {
          if (array_key_exists($data['original_language'], $unique_values)) {
            $form_state->setErrorByName('mappings][new_mapping][original_language', $this->t('Language mappings must be unique'));
          }
          $unique_values[] = ['original_language' => "{$data['original_language']}", 'copy_language' => "{$data['copy_language']}"];
        }
      }
    }

    $data = $form_state->getValue('new_mapping');
    \Drupal::logger('lingotek_copy_target')->info('data %d unique %u', ['%d' => $data['original_language'], '%u' => $data['copy_language']]);
    if (!empty($data['original_language']) && !empty($data['copy_language'])) {
      if ($data['original_language'] === $data['copy_language']) {
        $form_state->setErrorByName('mappings][new_mapping][original_language', $this->t('Language cannot be mapped to itself'));
      }
      elseif ($data['original_language'] === '' || $data['copy_language'] === '') {
        $form_state->setErrorByName('mappings][new_mapping][original_language', $this->t('Language mapping must consist of two languages'));
      }
      $unique_values[] = ['original_language' => "{$data['original_language']}", 'copy_language' => "{$data['copy_language']}"];
    }
    $form_state->set('mappings', $unique_values);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mappings = $form_state->get('mappings');
    if (!empty($mappings)) {
      $config = $this->config('lingotek_copy_target.mappings');
      $config->setData(['map' => $mappings]);
      $config->save();
    }
    parent::submitForm($form, $form_state);
  }

}
