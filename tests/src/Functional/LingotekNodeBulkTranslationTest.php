<?php

namespace Drupal\Tests\lingotek_copy_target\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\lingotek\Lingotek;
use Drupal\Tests\lingotek\Functional\LingotekTestBase;

/**
 * Tests translating a node using the bulk management form.
 *
 * @group lingotek_copy_target
 */
class LingotekNodeBulkTranslationTest extends LingotekTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'node', 'lingotek_copy_target'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    // Place the actions and title block.
    $this->drupalPlaceBlock('page_title_block', ['region' => 'content', 'weight' => -5]);
    $this->drupalPlaceBlock('local_tasks_block', ['region' => 'content', 'weight' => -10]);

    // Create Article node types.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Add a couple of languages.
    // Add some languages.
    ConfigurableLanguage::createFromLangcode('es')
      ->setName('Spanish - Mexico')
      ->setThirdPartySetting('lingotek', 'locale', 'es_MX')
      ->save();
    ConfigurableLanguage::createFromLangcode('es-AR')
      ->setName('Spanish - Argentina')
      ->setThirdPartySetting('lingotek', 'locale', 'es_AR')
      ->save();
    ConfigurableLanguage::createFromLangcode('it')
      ->setThirdPartySetting('lingotek', 'locale', 'it_IT')
      ->save();

    // Enable translation for the current entity type and ensure the change is
    // picked up.
    ContentLanguageSettings::loadByEntityTypeBundle('node', 'article')
      ->setLanguageAlterable(TRUE)
      ->save();
    \Drupal::service('content_translation.manager')
      ->setEnabled('node', 'article', TRUE);

    drupal_static_reset();
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    $this->applyEntityUpdates();
    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();

    $this->saveLingotekContentTranslationSettingsForNodeTypes();

    // Configure the mappings.
    $edit = [
      'new_mapping[original_language]' => 'es',
      'new_mapping[copy_language]' => 'es-AR',
    ];
    $this->drupalGet('admin/lingotek/settings/lingotek_copy_target/config');
    $this->submitForm($edit, 'Save configuration');

  }

  /**
   * Tests that a node can be translated using the links on the management page.
   */
  public function testNodeTranslationUsingLinks() {
    // Login as admin.
    $this->drupalLogin($this->rootUser);

    // Create a node.
    $edit = [];
    $edit['title[0][value]'] = 'Llamas are cool';
    $edit['body[0][value]'] = 'Llamas are very cool';
    $edit['langcode[0][value]'] = 'en';
    $edit['lingotek_translation_management[lingotek_translation_profile]'] = 'manual';
    $this->saveAndPublishNodeForm($edit);

    $this->goToContentBulkManagementForm();

    // Clicking English must init the upload of content.
    $this->assertLingotekUploadLink();
    // And we cannot request yet a translation.
    $this->assertNoLingotekRequestTranslationLink('es_MX');

    $this->clickLink('EN');
    $this->assertSession()->pageTextContains('Node Llamas are cool has been uploaded.');
    $this->assertSame('en_US', \Drupal::state()
      ->get('lingotek.uploaded_locale'));

    // There is a link for checking status.
    $this->assertLingotekCheckSourceStatusLink();
    // And we can already request a translation.
    $this->assertLingotekRequestTranslationLink('es_MX');

    $this->clickLink('EN');
    $this->assertSession()->pageTextContains('The import for node Llamas are cool is complete.');

    // Request the Spanish translation.
    $this->assertLingotekRequestTranslationLink('es_MX');
    $this->clickLink('ES');
    $this->assertSession()->pageTextContains("Locale 'es_MX' was added as a translation target for node Llamas are cool.");
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // Check status of the Spanish translation.
    $this->assertLingotekCheckTargetStatusLink('es_MX');
    $this->clickLink('ES');
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.checked_target_locale'));
    $this->assertSession()->pageTextContains('The es_MX translation for node Llamas are cool is ready for download.');

    // We can download the Spanish translation as a regular one.
    $this->assertLingotekDownloadTargetLink('es_MX');
    $this->clickLink('ES');
    $this->assertSession()->pageTextContains('The translation of node Llamas are cool into es_MX has been downloaded.');
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.downloaded_locale'));
    $this->assertTargetStatus('ES', Lingotek::STATUS_CURRENT);

    // And the Argentinian one has been created too.
    $this->assertTargetStatus('ES-AR', Lingotek::STATUS_UNTRACKED);

    $this->drupalGet('node/1/translations');
    $this->clickLink('Las llamas son chulas');

    $this->assertSession()->addressEquals('es-AR/node/1');
    $this->assertSession()->pageTextContains('Las llamas son chulas');
    $this->assertSession()->pageTextContains('Las llamas son muy chulas');
  }

}
