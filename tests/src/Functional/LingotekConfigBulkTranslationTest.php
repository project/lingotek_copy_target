<?php

namespace Drupal\Tests\lingotek_copy_target\Functional;

use Drupal\lingotek\Lingotek;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\lingotek\Functional\LingotekTestBase;
use Drupal\language\Entity\ContentLanguageSettings;

/**
 * Tests translation using the config bulk management form.
 *
 * @group lingotek_copy_target
 */
class LingotekConfigBulkTranslationTest extends LingotekTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'node', 'field_ui', 'lingotek_copy_target'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    // Create Article node types.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Add a couple of languages.
    // Add some languages.
    ConfigurableLanguage::createFromLangcode('es')
      ->setName('Spanish - Mexico')
      ->setThirdPartySetting('lingotek', 'locale', 'es_MX')
      ->save();
    ConfigurableLanguage::createFromLangcode('es-AR')
      ->setName('Spanish - Argentina')
      ->setThirdPartySetting('lingotek', 'locale', 'es_AR')
      ->save();
    ConfigurableLanguage::createFromLangcode('it')
      ->setThirdPartySetting('lingotek', 'locale', 'it_IT')
      ->save();

    ContentLanguageSettings::loadByEntityTypeBundle('node', 'article')
      ->setLanguageAlterable(TRUE)
      ->save();

    \Drupal::service('content_translation.manager')
      ->setEnabled('node', 'article', TRUE);

    drupal_static_reset();
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    $this->applyEntityUpdates();
    $this->rebuildContainer();
    $this->saveLingotekContentTranslationSettingsForNodeTypes();

    $edit = [
      'new_mapping[original_language]' => 'es',
      'new_mapping[copy_language]' => 'es-AR',
    ];
    $this->drupalGet('admin/lingotek/settings/lingotek_copy_target/config');
    $this->submitForm($edit, 'Save configuration');
    \Drupal::state()->set('lingotek.uploaded_content_type', 'body');
  }

  /**
   * Tests configuration translation using the links on the management form.
   */
  public function testConfigTranslationUsingLinks() {
    // Login as admin.
    $this->drupalLogin($this->rootUser);
    $this->goToConfigBulkManagementForm('node_fields');

    $basepath = \Drupal::request()->getBasePath();
    $this->assertSession()->pageTextContainsOnce('Article');
    // Upload the content.
    $this->clickLink('EN');
    $this->assertSame('en_US', \Drupal::state()
      ->get('lingotek.uploaded_locale'));

    // There is a link for checking status.
    $this->assertSession()->linkByHrefExists($basepath . '/admin/lingotek/config/check_upload/field_config/node.article.body?destination=' . $basepath . '/admin/lingotek/config/manage');
    // And we can already request a translation.
    $this->assertSession()->linkByHrefExists($basepath . '/admin/lingotek/config/request/field_config/node.article.body/es_MX?destination=' . $basepath . '/admin/lingotek/config/manage');
    $this->goToConfigBulkManagementForm('node_fields');
    $this->clickLink('EN');
    $this->assertSession()->pageTextContains('Body status checked successfully');

    // Request the Spanish translation.
    $this->clickLink('ES');
    $this->assertSession()->pageTextContains("Translation to es_MX requested successfully");
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // Check status of the Spanish translation.
    $this->clickLink('ES');
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.checked_target_locale'));
    $this->assertSession()->pageTextContains('Translation to es_MX status checked successfully');

    // There is a link for downloading.
    $this->assertSession()->linkByHrefExists($basepath . '/admin/lingotek/config/download/field_config/node.article.body/es_MX?destination=' . $basepath . '/admin/lingotek/config/manage');
    $this->clickLink('ES');
    $this->assertSession()->pageTextContains('Translation to es_MX downloaded successfully');
    $this->assertSame('es_MX', \Drupal::state()
      ->get('lingotek.downloaded_locale'));
    $this->assertTargetStatus('ES', Lingotek::STATUS_CURRENT);
    $this->assertTargetStatus('ES-AR', Lingotek::STATUS_UNTRACKED);
    $this->drupalGet('admin/config/regional/config-translation/node_fields');
    $this->assertSession()->pageTextContains('Body');
    $this->drupalGet('es/admin/config/regional/config-translation/node_fields');
    $this->assertSession()->pageTextContains('Cuerpo');
    $this->drupalGet('es-AR/admin/config/regional/config-translation/node_fields');
    $this->assertSession()->pageTextContains('Cuerpo');
  }

}
