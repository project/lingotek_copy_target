<?php

namespace Drupal\Tests\lingotek_copy_target\Functional\Form;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\lingotek\Functional\LingotekTestBase;

/**
 * Tests the mappings form.
 *
 * @group lingotek_copy_target
 */
class ConfigTargetsFormTest extends LingotekTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'lingotek_copy_target'];

  protected function setUp(): void {
    parent::setUp();

    // Add some languages.
    ConfigurableLanguage::createFromLangcode('es')
      ->setThirdPartySetting('lingotek', 'locale', 'es_ES')
      ->save();
    ConfigurableLanguage::createFromLangcode('es-MX')
      ->setName('Spanish - Mexico')
      ->setThirdPartySetting('lingotek', 'locale', 'es_MX')
      ->save();
    ConfigurableLanguage::createFromLangcode('es-AR')
      ->setName('Spanish - Argentina')
      ->setThirdPartySetting('lingotek', 'locale', 'es_AR')
      ->save();
    ConfigurableLanguage::createFromLangcode('es-419')
      ->setName('Spanish - Latin America and Caribbean')
      ->setThirdPartySetting('lingotek', 'locale', 'es_419')
      ->save();
    ConfigurableLanguage::createFromLangcode('it')
      ->setThirdPartySetting('lingotek', 'locale', 'it_IT')
      ->save();
    ConfigurableLanguage::createFromLangcode('de')
      ->setThirdPartySetting('lingotek', 'locale', 'de_DE')
      ->save();
  }

  public function testFormIsLinked() {
    $this->drupalGet('admin/config/regional/language/edit/es');
    $this->assertSession()->linkExists('Configure copy target');
    $this->assertSession()->linkByHrefExists('admin/lingotek/settings/lingotek_copy_target/config');
  }

  public function testAddingEditingRemovingMappings() {
    $this->drupalGet('admin/lingotek/settings/lingotek_copy_target/config');
    $this->assertSession()->pageTextContains('No mappings available');

    $edit = [
      'new_mapping[original_language]' => 'es',
      'new_mapping[copy_language]' => 'es-MX',
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // There is one row in the table.
    $this->assertSession()->elementsCount('css', 'table#edit-mappings tbody tr', 1);
    $this->assertSession()->fieldValueEquals('mappings[0][original_language]', 'es');
    $this->assertSession()->fieldValueEquals('mappings[0][copy_language]', 'es-MX');

    $edit = [
      'mappings[0][original_language]' => 'es-AR',
      'mappings[0][copy_language]' => 'es-MX',
      'new_mapping[original_language]' => 'es-AR',
      'new_mapping[copy_language]' => 'es-419',
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // There are two rows in the table.
    $this->assertSession()->elementsCount('css', 'table#edit-mappings tbody tr', 2);
    $this->assertSession()->fieldValueEquals('mappings[0][original_language]', 'es-AR');
    $this->assertSession()->fieldValueEquals('mappings[0][copy_language]', 'es-MX');
    $this->assertSession()->fieldValueEquals('mappings[1][original_language]', 'es-AR');
    $this->assertSession()->fieldValueEquals('mappings[1][copy_language]', 'es-419');

    // Delete the last one.
    $this->clickLink('Delete', 1);
    $this->assertSession()->pageTextContains('Are you sure you want to delete the mapping Spanish - Argentina (es-AR) → Spanish - Latin America and Caribbean (es-419)?');
    $this->submitForm([], 'Confirm');

    // There is only one row in the table.
    $this->assertSession()->elementsCount('css', 'table#edit-mappings tbody tr', 1);
    $this->assertSession()->fieldValueEquals('mappings[0][original_language]', 'es-AR');
    $this->assertSession()->fieldValueEquals('mappings[0][copy_language]', 'es-MX');
  }

}
